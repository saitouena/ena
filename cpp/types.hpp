#ifndef INCLUDED_TYPES
#define INCLUDED_TYPES

#include <string>
#include <vector>
#include <map>
#include <list>

#include "exception.hpp"

using namespace std;

class LispVal;
using Frame = map<string,LispVal>;
using Env = list<Frame>;

enum class TypeTag { List, Int, String, Symbol, PrimProc, Proc, Vector,True, False ,Nil};

class Closure {
private:
  int numParams;
  vector<string> params;
  vector<LispVal> body;
  Env env;
public:
  Closure(); // TODO: ?defualt constructor? (used to pass compiler check) see M-x flycheck-list-errors
  Closure(vector<LispVal>& expr, Env& env);
  ~Closure() {}
  vector<string> getParams() { return params; }
  vector<LispVal> getBody() { return body; }
  Env& getEnv() { return env; }
};

vector<string> makeParams(vector<LispVal>& expr);
vector<LispVal> makeBody(vector<LispVal>& expr);

Closure::Closure()
  : numParams {}, params {}, body {}, env {}
{
}

Closure::Closure(vector<LispVal>& expr, Env& e)
  : numParams {}, params {}, body {}, env {}
{
  params = makeParams(expr);
  numParams = static_cast<int>(params.size());
  body = makeBody(expr);
  env = e;
}

// TODO: more sufficient data structure (use union)
// TODO: ?implement move semantics?
class LispVal {
private:
  TypeTag type;
  string name; // for String, Symbol
  int num; // for Int
  vector<LispVal> elems; // for List,Vector
  Closure clos;
public:
  LispVal(TypeTag t, string s);
  LispVal(TypeTag t, int n);
  LispVal(TypeTag t, vector<LispVal> vs);
  LispVal(TypeTag t);
  LispVal();
  LispVal(vector<LispVal> expr, Env &env);
  LispVal(int n);
  ~LispVal() {}
  //  LispVal(const LispVal& v);
  // LispVal& operator=(const LispVal& v);
  //  LispVal(LispVal&& v);
  // LispVal& operator=(LispVal&& v);
  void push_back(LispVal v);
  TypeTag getType() { return type; }
  vector<LispVal> getElems() { return elems; }
  int getNum() { return num; }
  string getStr() { return name; }
  string getName() { return getStr(); }
  Closure getClos() { return clos; }
  bool isTrue();
  bool isFalse();
  bool isEmpty();
};

LispVal::LispVal(TypeTag t, string s)
  :type {}, name {}, num {}, elems {}, clos {}
{
  type = t;
  name = s;
}

LispVal::LispVal(TypeTag t, int n)
  :type {}, name {}, num {}, elems {}, clos {}
{
  type = t;
  num = n;
}

LispVal::LispVal(TypeTag t, vector<LispVal> ls)
  :type {}, name {}, num {}, elems {}, clos {} // vector, list
{
  type = t;
  elems = ls;
}

LispVal::LispVal()
  :type {}, name {}, num {}, elems {}, clos {}
{
  type = TypeTag::List;
}

LispVal::LispVal(TypeTag t)
  :type {}, name {}, num {}, elems {}, clos {}
{
  type = t;
}

LispVal::LispVal(vector<LispVal> expr, Env &env)
  :type {}, name {}, num {}, elems {}, clos {}
{
  type = TypeTag::Proc;
  clos = Closure(expr,env);
}

LispVal::LispVal(int n)
  :type {}, name {}, num {}, elems {}, clos {}
{
  type = TypeTag::Int;
  num = n;
}

// LispVal::LispVal(const LispVal& v) // copy constructor
//   :type {v.type}, name {v.name}, num {v.num}, elems {elems}, clos {clos}
// {
// }

// LispVal::LispVal(LispVal&& v)
//   :type {v.type}, name {v.name}, num {v.num}, elems {elems}, clos {clos}
// {
// }

// LispVal& LispVal::operator=(const LispVal& v) // copy constructor
// {
//   type = v.getType();
//   name = v.getName();
// }


void LispVal::push_back(LispVal v) {
  if(this->getType()!=TypeTag::List&&this->getType()!=TypeTag::Vector)
    throw InvalidOperation{};
  elems.push_back(v);
}

bool LispVal::isTrue() {
  if(this->getType()==TypeTag::False||this->getType()==TypeTag::Nil)
    return false;
  return true;
}

bool LispVal::isFalse() {
  return !(this->isTrue());
}

bool LispVal::isEmpty() {
  return (this->getType()==TypeTag::List&&this->getElems().size()==0);
}

LispVal make_closure(vector<LispVal> expr, Env &env) {
  return LispVal(expr,env);
}

vector<string> makeParams(vector<LispVal>&expr) { // TODO: error handling
  vector<LispVal> params = expr[1].getElems();
  vector<string> ret;
  for(auto& p : params) {
    if(p.getType()!=TypeTag::Symbol) {
      throw exception{};
    }
    ret.push_back(p.getName());
  }
  return ret;
}

vector<LispVal> makeBody(vector<LispVal> & expr) {
  vector<LispVal> ret;
  for(int i=2; i<static_cast<int>(expr.size()); i++) {
    ret.push_back(expr[i]);
  }
  return ret;
}

LispVal makeNil() { return LispVal(TypeTag::Nil); } // note: nil != (list)

bool list_p(LispVal v) {
  return v.getType()==TypeTag::List;
}

#endif
