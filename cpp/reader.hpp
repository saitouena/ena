#ifndef READER_INCLUDED
#define READER_INCLUDED

#include <string>
#include <vector>
#include <iostream>
#include <cctype>
#include <algorithm>
#include <sstream>

#include "types.hpp"
#include "tokenizer.hpp"
#include "utils.hpp"

using namespace std;

class Reader {
private:
  int pos;
  vector<string> tokens;

  string next() {
    pos++;
    return tokens[pos-1];
  }

  string peek() const {
    return tokens[pos];
  }

public:
  Reader(vector<string> tkns) :  pos{0}, tokens {tkns} {}
  ~Reader() {}
  LispVal read_form();
  LispVal read_list(LispVal ls); // ls is empty list
  LispVal read_atom();
  LispVal read_vector(LispVal vec);
};

LispVal read_str(string s) {
  Tokenizer tknzr(s);
  vector<string> tkns = tknzr.tokenize();
  Reader rdr(tkns);
  return rdr.read_form();
}

LispVal Reader::read_form() {
  if(peek()=="(") {
    next();
    return read_list(LispVal(TypeTag::List));
  }
  if(peek()=="[") {
    next();
    return read_vector(LispVal(TypeTag::Vector));
  }
  return read_atom();
}

LispVal Reader::read_list(LispVal acc) {
  if(peek()==")") {
    next();
    return acc;
  }
  LispVal v = read_form();
  acc.push_back(v);
  return read_list(acc);
}

// do like read_list
LispVal Reader::read_vector(LispVal acc) {
  if(peek()=="]") {
    next();
    return acc;
  }
  LispVal v = read_form();
  acc.push_back(v);
  return read_vector(acc);
}

LispVal Reader::read_atom() {
  string target = next();
  if(target.size()>=2&&target.front()=='\"'&&target.back()=='\"') {
    // string case
    return LispVal(TypeTag::String,target.substr(1,target.size()-2));
  }
  if(isnumber(target)) {
    istringstream iss(target);
    int ret;
    iss>>ret;
    return LispVal(TypeTag::Int,ret);
  }
  return LispVal(TypeTag::Symbol,target);
}

#endif // READER_INCLUDED

