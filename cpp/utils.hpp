#ifndef UTILS_INCLUDED
#define UTILS_INCLUDED
#include <string>
#include <cctype>

bool check_minus_number(std::string s);

// TODO: don't allow 00001123
bool isnumber(std::string s) {
  if(s[0]=='-')
    return check_minus_number(s);
  for(auto& ch : s) {
    if(!isdigit(ch))
      return false;
  }
  return true;
}

bool check_minus_number(std::string s) {
  // s[0]=='-'
  if(s.size()==1) return false; // "-" token
  for(int i=1; i<static_cast<int>(s.size()); i++)
    if(!isdigit(s[i])) return false;
  return true;
}

#endif
