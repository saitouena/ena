#ifndef PRINTER_INCLUDED
#define PRINTER_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "types.hpp"
#include "env.hpp"

using namespace std;

string pr_seq(LispVal v, string start, string end);
string pr_str(LispVal v);

string pr_seq(LispVal v, string start, string end) {
  stringstream ss;
  ss << start;
  vector<LispVal> elems = v.getElems();
  for(int i=0; i<static_cast<int>(elems.size()); i++) {
    if(i==0) {
      ss << pr_str(elems[i]);
    } else {
      ss<<" ";
      ss<<pr_str(elems[i]);
    }
  }
  ss << end;
  return ss.str();
}

string pr_env(Env env) {
  stringstream ss;
  int i=0;
  ss << "{";
  for(auto& f : env) {
    ss << "{frame"<<i;
    i++;
    ss<<" ";
    for(auto b  : f) {
      ss << b.first<< " : " << pr_str(b.second) <<",";
    }
    ss<<"}";
  }
  ss<<"}";
  return ss.str();
}

string pr_clos(Closure clos) {
  return "<#funciton env:" + pr_env(clos.getEnv()) + ">";
}

string pr_str(LispVal v) {
  auto vt = v.getType();
  if(vt==TypeTag::Nil)
    return "nil";
  if(vt==TypeTag::List) {
    return pr_seq(v, "(", ")");
  }
  if(vt==TypeTag::Vector) {
    return pr_seq(v,"[","]");
  }
  if(vt==TypeTag::Int) {
    stringstream ss;
    ss << v.getNum();
    return ss.str();
  }
  if(vt==TypeTag::String) {
    stringstream ss;
    ss<<"\"";
    ss << v.getStr();
    ss<<"\"";
    return ss.str();
  }
  if(vt==TypeTag::PrimProc) {
    string name = v.getName();
    stringstream ss;
    ss<<"#<primitive procedure(" << name<<")>";
    return ss.str();
  }
  if(vt==TypeTag::Proc) {
    return pr_clos(v.getClos());
  }
  if(vt==TypeTag::True)
    return "true";
  if(vt==TypeTag::False)
    return "false";
  // if Symbol
  return v.getStr();
}

#endif // PRINTER_INCLUDED
