#ifndef READLINE_INCLUDED
#define READLINE_INCLUDED

#include <string>
#include <readline/readline.h>
#include <readline/history.h>

bool readline(const std::string prompt, std::string& s);

// cf. https://eli.thegreenplace.net/2016/basics-of-using-the-readline-library/
// cf. https://github.com/kanaka/mal/blob/master/cpp/ReadLine.cpp
bool RL(const std::string &prompt, std::string& s) {
  char* buf = readline(prompt.c_str());
  if(buf==nullptr)
    return false;
  add_history(buf); // in memory
  s = std::string(buf);
  free(buf); // readline malloc buf
  return true;
}


#endif
