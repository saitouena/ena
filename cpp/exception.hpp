#ifndef INCLUDED_EXCEPTION
#define INCLUDED_EXCEPTION
#include <exception>
#include <string>

using namespace std;

struct UnbalancedParenthesis : public exception {};
struct InvalidOperation : public exception {};
struct NotApplicable : public exception {};
struct UnboundVariable : public exception
{
private:
  string varname;
public:
  UnboundVariable(string s);
  const string message() const noexcept {
    string ret = varname + " is not bound.";
    return ret;
  }
};
UnboundVariable::UnboundVariable(string s) : varname {s} {}
struct TypeMismatch : public exception {};
struct ZeroDivision : public exception {};
struct UnknownPrimitiveProcedure : public exception {};
struct ArgmentNumberMismatch : public exception {};
#endif // INCLUDED_EXCEPTION

