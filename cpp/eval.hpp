#ifndef INCLUDED_EVAL
#define INCLUDED_EVAL

#include <map>
#include "types.hpp"
#include "env.hpp"
#include "eval.hpp"
#include "exception.hpp"

using namespace std;

LispVal eval_primproc(LispVal ast, Env &env);
LispVal eval_sequence(vector<LispVal> seq, Env &env);
LispVal eval_ast(LispVal ast, Env &env);

bool special_check(LispVal fst,string keyword) {
  return (fst.getType()==TypeTag::Symbol&&fst.getName()==keyword);
}

#include "core.hpp"

LispVal eval_ast(LispVal ast, Env &env) {
  if(ast.getType()==TypeTag::Symbol) {
    string var = ast.getStr();
    if(checkVar(env,var))
      return getVar(env,var);
    throw UnboundVariable{var};
  }
  if(ast.getType()==TypeTag::List) {
    vector<LispVal> elems = ast.getElems();
    if(elems.size()==0) {
      return ast;
    }
    // TODO: more appropriate evaluation order.
    LispVal fst = elems[0];
    // def!
    if(special_check(fst,"def!")) { // define variable
      LispVal third = eval_ast(elems[2],env);
      defVar(env,elems[1].getStr(),third);
      return third;
    }
    // let*
    if(special_check(fst,"let*")) {
      Env newenv(env);
      newenv.push_front(Frame{});
      vector<LispVal> bindings = elems[1].getElems();
      for(int i=0; i<static_cast<int>(bindings.size());i+=2) { // TODO: throw error if bindings.size()!= even;
	string var = bindings[i].getStr();
	LispVal val = eval_ast(bindings[i+1],newenv);
	defVar(newenv,var,val);
      }
      return eval_ast(elems[2],newenv);
    }
    // do
    if(special_check(fst,"do")) {
      vector<LispVal> body;
      for(int i=1; i<static_cast<int>(elems.size()); i++) {
	body.push_back(elems[i]);
      }
      return eval_sequence(body,env);
    }
    // if
    if(special_check(fst,"if")) {
      LispVal p = elems[1];
      LispVal then = elems[2];
      if(eval_ast(p,env).isTrue()) {
	return eval_ast(then,env);
      }
      if(elems.size()<4)
	return makeNil();
      return eval_ast(elems[3],env);
    }
    // fn*
    if(special_check(fst,"fn*")) {
      return make_closure(elems,env);
    }
    fst = eval_ast(fst,env); // TODO: eval args first
    if(fst.getType()==TypeTag::PrimProc) // apply primitive procedure
      return eval_primproc(ast,env);
    // apply
    if(fst.getType()==TypeTag::Proc) {
      Closure clos = fst.getClos();
      vector<string> vars = clos.getParams();
      vector<LispVal> vals;
      for(int i=1; i<static_cast<int>(elems.size()); i++) {
	vals.push_back(eval_ast(elems[i],env));
      }
      if(vars.size()!=vals.size())
	throw ArgmentNumberMismatch {};
      
      Env newenv = extend_env(env, vars, vals); // TODO: fix this stmt after implement tco
      //      Env newenv = extend_env(clos.getEnv(), vars, vals); 
      vector<LispVal> body = clos.getBody();
      return eval_sequence(body,newenv);
    }
    throw NotApplicable{};
  }
  return ast;
}

LispVal eval_sequence(vector<LispVal> seq, Env & env) {
  int num_of_stmts = static_cast<int>(seq.size());
  for(int i=0; i<num_of_stmts-1; i++) {
    eval_ast(seq[i],env);
  }
  return eval_ast(seq[num_of_stmts-1],env);
}

LispVal eval_primproc(LispVal ast, Env &env) {
  // typetag of first element is PrimeProc (assured by caller)
  // TODO: refactoring (use generic accumulator)
  vector<LispVal> elems = ast.getElems();
  // eval args
  for(int i=1; i<static_cast<int>(elems.size()); i++) {
    elems[i] = eval_ast(elems[i],env);
  }
  auto fstElem = eval_ast(elems[0],env);
  if(fstElem.getStr()=="plus") {
    return core_plus(elems);
  }
  if(fstElem.getStr()=="minus") {
    return core_minus(elems);
  }
  if(fstElem.getStr()=="mult") {
    return core_mult(elems);
  }
  if(fstElem.getStr()=="div") {
    return core_div(elems);
  }
  if(fstElem.getStr()=="prn") {
    return core_prn(elems);
  }
  if(fstElem.getStr()=="list") {
    return core_list(elems);
  }
  if(fstElem.getStr()=="list?") {
    return core_list_p(elems);
  }
  if(fstElem.getStr()=="empty?") {
    return core_empty_p(elems);
  }
  if(fstElem.getStr()=="count") {
    return core_count(elems);
  }
  if(fstElem.getStr()=="=") {
    return core_eq(elems);
  }
  if(fstElem.getStr()=="<") {
    return core_gt(elems);
  }
  if(fstElem.getStr()=="<=") {
    return core_gte(elems);
  }
  if(fstElem.getStr()==">") {
    return core_lt(elems);
  }
  if(fstElem.getStr()==">=") {
    return core_lte(elems);
  }
  throw UnknownPrimitiveProcedure {};
}

#endif // INCLUDED_EVAL
