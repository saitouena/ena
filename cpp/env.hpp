#ifndef INCLUDED_ENV
#define INCLUDED_ENV

#include <map>
#include <list>

#include "types.hpp"

using namespace std;

using Frame = map<string,LispVal>;

using Env = list<Frame>;

namespace primitive {
  const LispVal plus(TypeTag::PrimProc,"plus");
  const LispVal minus(TypeTag::PrimProc,"minus");
  const LispVal mult(TypeTag::PrimProc,"mult");
  const LispVal div(TypeTag::PrimProc,"div");
  const LispVal prn(TypeTag::PrimProc,"prn");
  const LispVal list(TypeTag::PrimProc,"list");
  const LispVal list_p(TypeTag::PrimProc,"list?");
  const LispVal empty_p(TypeTag::PrimProc,"empty?");
  const LispVal count(TypeTag::PrimProc,"count");
  const LispVal eq(TypeTag::PrimProc,"=");
  const LispVal gt(TypeTag::PrimProc,"<");
  const LispVal gte(TypeTag::PrimProc,"<=");
  const LispVal lt(TypeTag::PrimProc,">");
  const LispVal lte(TypeTag::PrimProc,">=");
  const LispVal t_val(TypeTag::True);
  const LispVal f_val(TypeTag::False);
  const LispVal nil_val(TypeTag::Nil);
  
  const Frame primitives = {
    {"+", plus},
    {"-", minus},
    {"*", mult},
    {"/", div},
    {"prn",prn},
    {"list",list},
    {"list?",list_p},
    {"empty?",empty_p},
    {"count",count},
    {"=",eq},
    {"<",gt},
    {"<=",gte},
    {">",lt},
    {">=",lte},
    {"true",t_val},
    {"false",f_val},
    {"nil",nil_val},
  };
}

Env global_env{primitive::primitives,};

bool checkVar(Env &env, string var) {
  for(auto& f : env) {
    for(auto& p : f) {
      if(p.first==var) return true;
    }
  }
  return false;
}

LispVal getVar(Env &env, string var) {
  for(auto& f : env) {
    for(auto& p : f) {
      if(p.first==var)
	return p.second;
    }
  }
  throw exception{}; // you need assure getVar success with checkVar
}

// define variable in the first frame
void defVar(Env& env, string var, LispVal val) {
  env.front()[var]=val;
}

// sets variable in frame, or throws error if th var is not found
void setVar(Env& env, string var, LispVal val) {
  for(auto &f : env) {
    if(f.find(var)!=f.end()) {
      f[var]=val;
      return;
    }
  }
  throw UnboundVariable{var};
}

Frame make_frame(vector<string> &vars, vector<LispVal> &vals) {
  Frame f;
  int n = static_cast<int>(vars.size());
  if(n!=static_cast<int>(vals.size()))
    throw exception{};
  for(int i=0; i<n; i++)
    f[vars[i]]=vals[i];
  return f;
}

Env extend_env(Env& env, vector<string> &vars, vector<LispVal> &vals) {
  Frame f = make_frame(vars, vals);
  Env newenv(env);
  newenv.push_front(f);
  return newenv;
}

#endif // INCLUDED_ENV
