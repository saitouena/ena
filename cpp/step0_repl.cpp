#include <string>
#include <map>
#include <iostream>

std::string READ(std::string s) {
  return s;
}

std::string EVAL(std::string ast, std::map<std::string,std::string> env) { // env is stub for now.
  return ast;
}

std::string PRINT(std::string exp) {
  return exp;
}

std::string rep(std::string s) {
  return PRINT(EVAL(READ(s),std::map<std::string,std::string>{}));
}

bool readline(const std::string prompt, std::string& s) {
  if(std::cin.eof())
    return false;
  std::cout << prompt;
  getline(std::cin,s);
  return true;
}

int main() {
  std::string s;
  while(readline("user> ",s)) {
    std::cout << rep(s) << std::endl;
  }
  return 0;
}
