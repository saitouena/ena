#include <string>
#include <map>
#include <iostream>

#include "types.hpp"
#include "reader.hpp"
#include "printer.hpp"
#include "env.hpp"
#include "eval.hpp"
#include "eval.cpp"

LispVal READ(std::string s) {
  return read_str(s);
}

LispVal EVAL(LispVal ast, Env env) { // env is stub for now.
  return eval_ast(ast,env);
}

string PRINT(LispVal val) {
  return pr_str(val);
}

std::string rep(std::string s) {
  LispVal v = EVAL(READ(s),global_env);
  return PRINT(v);
}

bool readline(const std::string prompt, std::string& s) {
  if(std::cin.eof())
    return false;
  std::cout << prompt;
  getline(std::cin,s);
  return true;
}

int main() {
  std::string s;
  while(true) {
    try {
      if(!readline("user> ",s)) {
	cout << endl << "bye."<<endl;
	exit(0);
      }
      if(!s.empty())
	std::cout << rep(s) << std::endl;
    }
    // TODO: make error itself to say what happened.
    catch (UnbalancedParenthesis& e) {
      cout << "unbound parentheis" << endl;
    } catch (InvalidOperation& e) {
      cout << "invalid operation" << endl;
    } catch (NotApplicable& e) {
      cout << "not applicable" << endl;
    } catch (UnboundVariable& e) {
      cout << "unbound variable" << endl;
    } catch (TypeMismatch& e) {
      cout << "type mismatch" << endl;
    } catch (ZeroDivision& e) {
      cout << "zero division" << endl;
    } catch (UnknownPrimitiveProcedure& e) {
      cout << "unknown primitive procedure" << endl;
    } catch (exception& e) {
      cout << "exception" << endl;
    }
  }
  return 0;
}
