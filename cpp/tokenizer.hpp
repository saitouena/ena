#include <vector>
#include <string>
#include <unordered_set>
#include <stack>

#include "exception.hpp"

using namespace std;

class Tokenizer {
private:
  string str;
public:
  Tokenizer(string s) : str{s} {}
  vector<string> tokenize();
};

const unordered_set<char> skiplist = { '\n', ' ', '\t', '\r', ','};

// TODO: quotes, macros, [], pair expr like ( . )
vector<string> Tokenizer::tokenize() {
  stack<char> stck;
  vector<string> ret;
  string acc;
  bool stringparsing=false;
  for(auto& ch : str) {
    if(stringparsing) {
      if(ch=='\"') {
	stringparsing = false;
	acc+=ch;
	ret.push_back(acc);
	acc="";
      } else {
	acc+=ch;
      }
      continue;
    }
    if(skiplist.find(ch)!=skiplist.end()) {
      if(!acc.empty()) {
	ret.push_back(acc);
	acc="";
      }
    } else if(ch=='('||ch=='[') {
      if(!acc.empty()) {
	ret.push_back(acc);
	acc="";
      }
      ret.push_back(string{ch});
      stck.push(ch);
    } else if(ch==')'||ch==']') {
      if(!acc.empty()) {
	ret.push_back(acc);
	acc="";
      }
      if(!stck.empty()) {
	if(stck.top()=='('&&ch==')')
	  stck.pop();
	else if(stck.top()=='['&&ch==']')
	  stck.pop();
	else
	  throw UnbalancedParenthesis{};
      } else {
	throw UnbalancedParenthesis{};
      }
      ret.push_back(string{ch});
    } else if(ch=='\"') { // string
      stringparsing = true;
      acc+=ch;
    } else { // integers, atoms
      acc+=ch;
    }
  }
  if(!stck.empty())
      throw UnbalancedParenthesis{};
  if(!acc.empty())
    ret.push_back(acc);
  return ret;
}
