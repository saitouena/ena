#ifndef INCLUDED_CORE
#define INCLUDED_CORE

#include <vector>

#include "types.hpp"

LispVal core_plus(vector<LispVal> expr);
LispVal core_minus(vector<LispVal> expr);
LispVal core_mult(vector<LispVal> elems);
LispVal core_div(vector<LispVal> elems);
LispVal core_prn(vector<LispVal> elems);
LispVal core_list(vector<LispVal> elems);
LispVal core_list_p(vector<LispVal> elems);
LispVal core_empty_p(vector<LispVal> elems);
LispVal core_count(vector<LispVal> elems);
LispVal core_eq(vector<LispVal> elems);
LispVal core_gt(vector<LispVal> elems);
LispVal core_gte(vector<LispVal> elems);
LispVal core_lt(vector<LispVal> elems);
LispVal core_lte(vector<LispVal> elems);

#include "eval.hpp"
#include "printer.hpp"

void argmentCheck(vector<LispVal> expr, int n) {
  if(static_cast<int>(expr.size())!=n)
    throw ArgmentNumberMismatch {};
}

void typeCheck(LispVal expr, TypeTag tag) {
  if(tag!=expr.getType())
    throw TypeMismatch {};
}

LispVal core_plus(vector<LispVal> expr) {
  int acc=0;
  for(auto i=1; i<static_cast<int>(expr.size()); i++) {
    auto v = expr[i];
    if(v.getType()==TypeTag::Int) {
      acc+=v.getNum();
    } else {
      throw TypeMismatch{};
    }
  }
  return LispVal(TypeTag::Int, acc);
}

LispVal core_minus(vector<LispVal> elems) {
  if(elems.size()==1)
    return LispVal(TypeTag::Int,0);
  auto second = elems[1];
  int acc=0;
  if(second.getType()==TypeTag::Int) {
    acc+=second.getNum();
  } else {
    throw TypeMismatch{};
  }
  for(auto i=2; i<static_cast<int>(elems.size()); i++) {
    auto v = elems[i];
    if(v.getType()==TypeTag::Int) {
      acc-=v.getNum();
    } else {
      throw TypeMismatch{};
    }
  }
  return LispVal(TypeTag::Int, acc);
}

LispVal core_mult(vector<LispVal> elems) {
  int acc=1;
  for(auto i=1; i<static_cast<int>(elems.size()); i++) {
    auto v = elems[i];
    if(v.getType()==TypeTag::Int) {
      acc*=v.getNum();
    } else {
      throw TypeMismatch{};
    }
  }
  return LispVal(TypeTag::Int, acc);
}

LispVal core_div(vector<LispVal> elems) {
  if(elems.size()==1) // TODO: valid?
    return LispVal(TypeTag::Int,0);
  auto second = elems[1];
  int acc=0;
  if(second.getType()==TypeTag::Int) {
    acc+=second.getNum();
  } else {
    throw TypeMismatch{};
  }
  for(auto i=2; i<static_cast<int>(elems.size()); i++) {
    auto v = elems[i];
    if(v.getType()==TypeTag::Int) {
      if(v.getNum()==0)
	throw ZeroDivision{};
      acc/=v.getNum();
    } else {
      throw TypeMismatch{};
    }
  }
  return LispVal(TypeTag::Int, acc);
}

LispVal core_prn(vector<LispVal> elems) {
  if(elems.size()!=2)
    throw ArgmentNumberMismatch {};
  cout << pr_str(elems[1]) << endl;
  return makeNil();
}

LispVal core_list(vector<LispVal> elems) {
  auto ret = LispVal();
  if(elems.size()==1)
    return ret;
  for(int i=1; i<static_cast<int>(elems.size()); i++) {
    ret.push_back(elems[i]);
  }
  return ret;
}

LispVal core_list_p(vector<LispVal> elems) {
  argmentCheck(elems,2);
  if(list_p(elems[1]))
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

LispVal core_empty_p(vector<LispVal> elems) {
  argmentCheck(elems,2);
  if(elems[1].isEmpty())
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

LispVal core_count(vector<LispVal> exps) {
  argmentCheck(exps,2);
  if(exps[1].getType()==TypeTag::Nil)
    return LispVal(0);
  typeCheck(exps[1],TypeTag::List);
  return LispVal(exps[1].getElems().size());
}

bool internal_eq(LispVal a, LispVal b) {
  TypeTag at = a.getType();
  TypeTag bt = b.getType();
  if(at!=bt)
    return false;
  if(at==TypeTag::True||at==TypeTag::False||at==TypeTag::Nil)
    return true;
  if(at==TypeTag::Int) {
    if(a.getNum()==b.getNum())
      return true;
    else
      return false;
  }
  if(at==TypeTag::String) {
    if(a.getStr()==b.getStr())
      return true;
    else
      return false;
  }
  if(at==TypeTag::Symbol||at==TypeTag::PrimProc) {
    if(a.getName()==b.getName())
      return true;
    else
      return false;
  }
  if(at==TypeTag::List||at==TypeTag::Vector) {
    if(a.getElems().size()!=b.getElems().size())
      return false;
    vector<LispVal> as = a.getElems();
    vector<LispVal> bs = b.getElems();
    for(int i=0; i<static_cast<int>(as.size()); i++) {
      if(!internal_eq(as[i],bs[i])) return false;
    }
    return true;
  }
  return false;
}

LispVal core_eq(vector<LispVal> exps) {
  argmentCheck(exps,3);
  LispVal a = exps[1];
  LispVal b = exps[2];
  if(internal_eq(a,b))
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

// a < b
LispVal core_gt(vector<LispVal> exps) {
  argmentCheck(exps,3);
  LispVal a = exps[1];
  LispVal b = exps[2];
  typeCheck(a,TypeTag::Int);
  typeCheck(b,TypeTag::Int);
  if(a.getNum()<b.getNum())
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

// a<=b
LispVal core_gte(vector<LispVal> exps) {
  argmentCheck(exps,3);
  LispVal a = exps[1];
  LispVal b = exps[2];
  typeCheck(a,TypeTag::Int);
  typeCheck(b,TypeTag::Int);
  if(a.getNum()<=b.getNum())
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

// a>b
LispVal core_lt(vector<LispVal> exps) {
  argmentCheck(exps,3);
  LispVal a = exps[1];
  LispVal b = exps[2];
  typeCheck(a,TypeTag::Int);
  typeCheck(b,TypeTag::Int);
  if(a.getNum()>b.getNum())
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

// a>=b
LispVal core_lte(vector<LispVal> exps) {
  argmentCheck(exps,3);
  LispVal a = exps[1];
  LispVal b = exps[2];
  typeCheck(a,TypeTag::Int);
  typeCheck(b,TypeTag::Int);
  if(a.getNum()>=b.getNum())
    return LispVal(TypeTag::True);
  return LispVal(TypeTag::False);
}

#endif
